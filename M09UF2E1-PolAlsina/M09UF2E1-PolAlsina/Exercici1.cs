﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace M09UF2E1_PolAlsina
{
    /*
        Exercici 1: esbrinar quantes línies té un fitxer de text (grans i petits)
        escriu una funció que obri un fitxer de text i compti quantes línies té.
        crea un Thread que compti i mostri quantes línies té un fitxer (reutilitza el codi anterior). El nom del fitxer serà un atribut del Thread de tipus String.
        Crea una classe de Prova que crida a 3 threads per esbrinar quantes línies té cada fitxer. Proveu amb fitxers grans i petits.
    */
    class Exercici1
    {
        static void Main()
        {
            Directory.SetCurrentDirectory("..\\..\\..");
            Console.WriteLine(Directory.GetCurrentDirectory());
            Threads();
        }
        static void Threads()
        {
            Thread a = new Thread(ReadLinesTxt);
            a.Start("Thread.txt");
            Thread b = new Thread(ReadLinesTxt);
            b.Start("Thread2.txt");
            Thread c = new Thread(ReadLinesTxt);
            c.Start("Thread3.txt");
        }
       public static void ReadLinesTxt(Object path)
        { 
            string Parameter = (string)path;
            using (StreamReader file = new StreamReader(Parameter))
            {
                int counter = 0;
                string ln;

                while ((ln = file.ReadLine()) != null)
                {                
                    counter++;
                }
                file.Close();
                Console.WriteLine("The file has: " + counter+" lines.");
            }
        }
    }
}
